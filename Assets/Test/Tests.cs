﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Tests : MonoBehaviour
{
    Custom_Test ct_Object;
    SC_Test sc_Object;
    void Start()
    {
        UnityEngine.Debug.Log($"Total Memory antes: {GC.GetTotalMemory(true)}");
        GC.Collect();
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        //Test de scriptable object
        sc_Object = Resources.Load<SC_Test>("test");
        double result = sc_Object.number + sc_Object.number2;

        ////Test de custom class 

        //double result = ct_Object.number + ct_Object.number2;



        UnityEngine.Debug.Log($"Total Memory: {GC.GetTotalMemory(true)}");

        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log(ts.TotalMilliseconds);

        ct_Object = new Custom_Test();
        Invoke("Test_CustomClass", 3);

    }

    void Test_CustomClass()
    {
        GC.Collect();
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        Custom_Test ct_Object = new Custom_Test();
        double result = ct_Object.number + ct_Object.number2;

        UnityEngine.Debug.Log($"Total Memory: {GC.GetTotalMemory(true)}");

        stopWatch.Stop();
        TimeSpan ts = stopWatch.Elapsed;
        UnityEngine.Debug.Log(ts.TotalMilliseconds);
    }
}

//Crear y guardar de manera permanente el estado de la clase custom
//Custom_Test custom_Test = new Custom_Test();
//custom_Test.SaveValues();