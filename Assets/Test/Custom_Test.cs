﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using UnityEngine;

[Serializable]
public class Custom_Test 
{
    public int number;
    public string name;
    public double number2;

    public Custom_Test()
    {
        LoadValues();
    }

    public void LoadValues()
    {
        string destination = Application.streamingAssetsPath + "/banana.test";
        FileStream file;

        if (File.Exists(destination)) file = File.OpenRead(destination);
        else
        {
            Debug.LogError("File not found");
            return;
        }

        BinaryFormatter bf = new BinaryFormatter();
        Custom_Test data = (Custom_Test)bf.Deserialize(file);
        name = data.name;
        number = data.number;
        number2 = data.number2;
        file.Close();


    }
    public void SaveValues()
    {
        string destination = Application.streamingAssetsPath + "/banana.test";
        FileStream file;

         file = File.Create(destination);

        Custom_Test data = new Custom_Test();
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, data);
        file.Close();
    }


}
